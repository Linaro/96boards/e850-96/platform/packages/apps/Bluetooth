/*
 * Copyright 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.bluetooth.avrcp;

import android.bluetooth.BluetoothA2dp;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.IBluetoothAvrcpTarget;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.AudioPlaybackConfiguration;
import android.media.session.PlaybackState;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.SystemProperties;
import android.os.UserManager;
import android.util.Log;

import com.android.bluetooth.BluetoothMetricsProto;
import com.android.bluetooth.Utils;
import com.android.bluetooth.a2dp.A2dpService;
import com.android.bluetooth.btservice.MetricsLogger;
import com.android.bluetooth.btservice.ProfileService;
import com.android.bluetooth.btservice.ServiceFactory;

import java.util.List;
import java.util.Objects;

/**
 * Provides Bluetooth AVRCP Target profile as a service in the Bluetooth application.
 * @hide
 */
public class AvrcpTargetService extends ProfileService {
    private static final String TAG = "AvrcpTargetService";
    private static final boolean DEBUG = true;
    private static final String AVRCP_ENABLE_PROPERTY = "persist.bluetooth.enablenewavrcp";

    private static final int AVRCP_MAX_VOL = 127;
    private static int sDeviceMaxVolume = 0;

    private MediaPlayerList mMediaPlayerList;
    private AudioManager mAudioManager;
    private AvrcpBroadcastReceiver mReceiver;
    private AvrcpNativeInterface mNativeInterface;
    private AvrcpVolumeManager mVolumeManager;
    private ServiceFactory mFactory = new ServiceFactory();

    private Handler mAudioManagerPlaybackHandler;
    private AudioManagerPlaybackListener mAudioManagerPlaybackCb;
    private boolean mAudioManagerIsPlaying;

    // Only used to see if the metadata has changed from its previous value
    private MediaData mCurrentData;

    private static AvrcpTargetService sInstance = null;

    private static final String playerStateUpdateBlackListedAddr[] = {
            "9C:DF:03", //9C:DF:03:D3:C0:17, Name: Benz S600L
            "00:04:79", //00-04-79-00-06-bc, Name: radius HP-BTL01
    };
    private static final String playerStateUpdateBlackListedNames[] = {
            "Audi",
            "Porsche",
            "BMW",
            "VW",
    };
    private static final String browsedPlayerBlackListedAddr[] = {
    };
    private static final String browsedPlayerBlackListedNames[] = {
            "BMW",
            "LEXUS",
    };

    class ListCallback implements MediaPlayerList.MediaUpdateCallback,
            MediaPlayerList.FolderUpdateCallback {
        @Override
        public void run(MediaData data) {
            boolean metadata = !Objects.equals(mCurrentData.metadata, data.metadata);
            boolean state = !MediaPlayerWrapper.playstateEquals(mCurrentData.state, data.state);
            boolean queue = !Objects.equals(mCurrentData.queue, data.queue);

            if (DEBUG) {
                Log.d(TAG, "onMediaUpdated: track_changed=" + metadata
                        + " state=" + state + " queue=" + queue);
            }
            mCurrentData = data;

            //Check some conditions and do not proceed sendMediaUpdate()
            BluetoothDevice a2dpActiveDevice = (A2dpService.getA2dpService() != null) ?
                    A2dpService.getA2dpService().getActiveDevice() : null;
            if (state && a2dpActiveDevice != null && mCurrentData.state != null) {
                Log.d(TAG, "Current media playbackstate: " + mCurrentData.state);
                if (isPlayerStateUpdateBlackListed(a2dpActiveDevice.getAddress(),
                        a2dpActiveDevice.getName()) == true) {
                    if (mCurrentData.state.getState() == PlaybackState.STATE_STOPPED ||
                            mCurrentData.state.getState() == PlaybackState.STATE_PAUSED) {
                        if (mAudioManagerIsPlaying) {
                            if ((A2dpService.getA2dpService().getConnectedDevices().size() > 0) &&
                                    A2dpService.getA2dpService().isA2dpPlaying(a2dpActiveDevice)) {
                                Log.i(TAG, "A2dp is playing : skip updating playbackstate");
                            } else {
                                Log.i(TAG, "Merged playbackConfig is active :" +
                                        " skip updating playbackstate");
                            }
                            state = false;
                        }
                    } else if (mCurrentData.state.getState() == PlaybackState.STATE_BUFFERING
                            || mCurrentData.state.getState() == PlaybackState.STATE_NONE) {
                        Log.i(TAG, "Skip updating invalid playbackstate : " +
                                mCurrentData.state.getState());
                        state = false;
                    }
                }
            }

            mNativeInterface.sendMediaUpdate(metadata, state, queue);
        }

        @Override
        public void run(boolean availablePlayers, boolean addressedPlayers,
                boolean uids) {
            mNativeInterface.sendFolderUpdate(availablePlayers, addressedPlayers, uids);
        }
    }

    private class AvrcpBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(BluetoothA2dp.ACTION_ACTIVE_DEVICE_CHANGED)) {
                if (mNativeInterface == null) return;

                // Update all the playback status info for each connected device
                mNativeInterface.sendMediaUpdate(false, true, false);
            } else if (action.equals(BluetoothA2dp.ACTION_CONNECTION_STATE_CHANGED)) {
                if (mNativeInterface == null) return;

                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (device == null) return;

                int state = intent.getIntExtra(BluetoothProfile.EXTRA_STATE, -1);
                if (state == BluetoothProfile.STATE_DISCONNECTED) {
                    // If there is no connection, disconnectDevice() will do nothing
                    if (mNativeInterface.disconnectDevice(device.getAddress())) {
                        Log.d(TAG, "request to disconnect device " + device);
                    }
                }
            } else if (action.equals(BluetoothA2dp.ACTION_PLAYING_STATE_CHANGED)) {
                if (mNativeInterface == null) return;

                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                BluetoothDevice a2dpActiveDevice = (A2dpService.getA2dpService() != null) ?
                        A2dpService.getA2dpService().getActiveDevice() : null;

                if (device == null || a2dpActiveDevice == null ||
                        !device.equals(a2dpActiveDevice)) return;

                int newState = intent.getIntExtra(BluetoothProfile.EXTRA_STATE, -1);
                int oldState = intent.getIntExtra(BluetoothProfile.EXTRA_PREVIOUS_STATE, -1);

                Log.d(TAG,"a2dp playing status: " + audioStateToString(oldState) + " -> " +
                        audioStateToString(newState));
                if (isPlayerStateUpdateBlackListed(a2dpActiveDevice.getAddress(),
                        a2dpActiveDevice.getName()) == true) {
                    if (oldState != newState) {
                        Log.d(TAG,"sendMediaUpdate to update playstatus " +
                                "forcely with blacklisted CT\n" +
                                "Current media playbackstate: " + mCurrentData.state);
                        mNativeInterface.sendA2dpPlayStatusUpdate();
                    }
                }
            }
        }
    }

    private static String audioStateToString(int state) {
        switch (state) {
            case BluetoothA2dp.STATE_NOT_PLAYING:
                return "NOT_PLAYING";
            case BluetoothA2dp.STATE_PLAYING:
                return "PLAYING";
            default:
                break;
        }
        return "UNKNOWN";
    }

    private class AudioManagerPlaybackListener extends AudioManager.AudioPlaybackCallback {
        @Override
        public void onPlaybackConfigChanged(List<AudioPlaybackConfiguration> configs) {
            Log.d(TAG, "onPlaybackConfigChanged: configs list size = " + configs.size());
            boolean isActive = false;
            for (AudioPlaybackConfiguration config : configs) {
                Log.d(TAG, "onPlaybackConfigChanged: config = "
                        + AudioPlaybackConfiguration.toLogFriendlyString(config));
                isActive |= config.isActive();
            }
            mAudioManagerIsPlaying = isActive;
            Log.d(TAG, "onPlaybackConfigChanged: mAudioManagerIsPlaying = "
                    + mAudioManagerIsPlaying);
        }
    }

    /**
     * Get the AvrcpTargetService instance. Returns null if the service hasn't been initialized.
     */
    public static AvrcpTargetService get() {
        return sInstance;
    }

    @Override
    public String getName() {
        return TAG;
    }

    @Override
    protected IProfileServiceBinder initBinder() {
        return new AvrcpTargetBinder(this);
    }

    @Override
    protected void setUserUnlocked(int userId) {
        Log.i(TAG, "User unlocked, initializing the service");

        if (!SystemProperties.getBoolean(AVRCP_ENABLE_PROPERTY, true)) {
            Log.w(TAG, "Skipping initialization of the new AVRCP Target Player List");
            sInstance = null;
            return;
        }

        if (mMediaPlayerList != null) {
            mMediaPlayerList.init(new ListCallback());
        }
    }

    @Override
    protected boolean start() {
        if (sInstance != null) {
            Log.wtfStack(TAG, "The service has already been initialized");
            return false;
        }

        Log.i(TAG, "Starting the AVRCP Target Service");
        mCurrentData = new MediaData(null, null, null);

        if (!SystemProperties.getBoolean(AVRCP_ENABLE_PROPERTY, true)) {
            Log.w(TAG, "Skipping initialization of the new AVRCP Target Service");
            sInstance = null;
            return true;
        }

        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        sDeviceMaxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);

        mMediaPlayerList = new MediaPlayerList(Looper.myLooper(), this);

        mNativeInterface = AvrcpNativeInterface.getInterface();
        mNativeInterface.init(AvrcpTargetService.this);

        mVolumeManager = new AvrcpVolumeManager(this, mAudioManager, mNativeInterface);

        UserManager userManager = UserManager.get(getApplicationContext());
        if (userManager.isUserUnlocked()) {
            mMediaPlayerList.init(new ListCallback());
        }

        HandlerThread thread = new HandlerThread("BluetoothAvrcpAudioPlaybackHandler");
        thread.start();
        mAudioManagerPlaybackHandler = new Handler(thread.getLooper());
        mAudioManagerPlaybackCb = new AudioManagerPlaybackListener();
        mAudioManager.registerAudioPlaybackCallback(
                mAudioManagerPlaybackCb, mAudioManagerPlaybackHandler);

        mReceiver = new AvrcpBroadcastReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothA2dp.ACTION_ACTIVE_DEVICE_CHANGED);
        filter.addAction(BluetoothA2dp.ACTION_CONNECTION_STATE_CHANGED);
        filter.addAction(BluetoothA2dp.ACTION_PLAYING_STATE_CHANGED);
        registerReceiver(mReceiver, filter);

        mAudioManagerIsPlaying = false;

        // Only allow the service to be used once it is initialized
        sInstance = this;

        return true;
    }

    @Override
    protected boolean stop() {
        Log.i(TAG, "Stopping the AVRCP Target Service");

        if (sInstance == null) {
            Log.w(TAG, "stop() called before start()");
            return true;
        }

        sInstance = null;
        unregisterReceiver(mReceiver);

        mAudioManager.unregisterAudioPlaybackCallback(mAudioManagerPlaybackCb);
        mAudioManagerPlaybackHandler.removeCallbacksAndMessages(null);

        // We check the interfaces first since they only get set on User Unlocked
        if (mMediaPlayerList != null) mMediaPlayerList.cleanup();
        if (mNativeInterface != null) mNativeInterface.cleanup();
        if (mVolumeManager != null) mVolumeManager.cleanup();

        mMediaPlayerList = null;
        mNativeInterface = null;
        mAudioManager = null;
        mVolumeManager = null;
        mAudioManagerPlaybackHandler = null;
        mReceiver = null;
        return true;
    }

    private void init() {
    }

    void deviceConnected(BluetoothDevice device, boolean absoluteVolume) {
        Log.i(TAG, "deviceConnected: device=" + device + " absoluteVolume=" + absoluteVolume);
        mVolumeManager.deviceConnected(device, absoluteVolume);
        MetricsLogger.logProfileConnectionEvent(BluetoothMetricsProto.ProfileId.AVRCP);
    }

    void deviceDisconnected(BluetoothDevice device) {
        Log.i(TAG, "deviceDisconnected: device=" + device);
        mVolumeManager.deviceDisconnected(device);
    }

    /**
     * Signal to the service that the current audio out device has changed and to inform
     * the audio service whether the new device supports absolute volume. If it does, also
     * set the absolute volume level on the remote device.
     */
    public void volumeDeviceSwitched(BluetoothDevice device) {
        if (DEBUG) {
            Log.d(TAG, "volumeDeviceSwitched: device=" + device);
        }
        mVolumeManager.volumeDeviceSwitched(device);
    }

    boolean isPlayerStateUpdateBlackListed(String address, String deviceName) {
        if (address == null && deviceName == null) {
            Log.i(TAG, "isPlayerStateUpdateBlackListed: invalid parameters (null)");
            return false;
        }

        if (address != null) {
            for (int i = 0; i < playerStateUpdateBlackListedAddr.length; i++) {
                String addr = playerStateUpdateBlackListedAddr[i];
                if (address.toLowerCase().startsWith(addr.toLowerCase())) {
                    Log.d(TAG, "isPlayerStateUpdateBlackListed Addr: " + address);
                    return true;
                }
            }
        }

        if (deviceName != null) {
            for (int j = 0; j < playerStateUpdateBlackListedNames.length; j++) {
                String name = playerStateUpdateBlackListedNames[j];
                if (deviceName.toLowerCase().startsWith(name.toLowerCase())) {
                    Log.d(TAG, "isPlayerStateUpdateBlackListed Name: " + deviceName);
                    return true;
                }
            }
        }
        return false;
    };

    boolean isBrowsedPlayerBlackListed(String address, String deviceName) {
        if (address == null && deviceName == null) {
            Log.i(TAG, "isBrowsedPlayerBlackListed: invalid parameters (null)");
            return false;
        }

        if (address != null) {
            for (int i = 0; i < browsedPlayerBlackListedAddr.length; i++) {
                String addr = browsedPlayerBlackListedAddr[i];
                if (address.toLowerCase().startsWith(addr.toLowerCase())) {
                    return true;
                }
            }
        }

        if (deviceName != null) {
            for (int j = 0; j < browsedPlayerBlackListedNames.length; j++) {
                String name = browsedPlayerBlackListedNames[j];
                if (deviceName.toLowerCase().startsWith(name.toLowerCase())) {
                    return true;
                }
            }
        }
        return false;
    };

    /**
     * Store the current system volume for a device in order to be retrieved later.
     */
    public void storeVolumeForDevice(BluetoothDevice device) {
        if (device == null) return;

        List<BluetoothDevice> HAActiveDevices = null;
        if (mFactory.getHearingAidService() != null) {
            HAActiveDevices = mFactory.getHearingAidService().getActiveDevices();
        }
        if (HAActiveDevices != null
                && (HAActiveDevices.get(0) != null || HAActiveDevices.get(1) != null)) {
            Log.d(TAG, "Do not store volume when Hearing Aid devices is active");
            return;
        }
        mVolumeManager.storeVolumeForDevice(device);
    }

    /**
     * Remove the stored volume for a device.
     */
    public void removeStoredVolumeForDevice(BluetoothDevice device) {
        if (device == null) return;

        mVolumeManager.removeStoredVolumeForDevice(device);
    }

    /**
     * Retrieve the remembered volume for a device. Returns -1 if there is no volume for the
     * device.
     */
    public int getRememberedVolumeForDevice(BluetoothDevice device) {
        if (device == null) return -1;

        return mVolumeManager.getVolume(device, mVolumeManager.getNewDeviceVolume());
    }

    // TODO (apanicke): Add checks to blacklist Absolute Volume devices if they behave poorly.
    void setVolume(int avrcpVolume) {
        BluetoothDevice activeDevice = mFactory.getA2dpService().getActiveDevice();
        if (activeDevice == null) {
            Log.d(TAG, "setVolume: no active device");
            return;
        }

        mVolumeManager.setVolume(activeDevice, avrcpVolume);
    }

    /**
     * Set the volume on the remote device. Does nothing if the device doesn't support absolute
     * volume.
     */
    public void sendVolumeChanged(int deviceVolume) {
        BluetoothDevice activeDevice = mFactory.getA2dpService().getActiveDevice();
        if (activeDevice == null) {
            Log.d(TAG, "sendVolumeChanged: no active device");
            return;
        }

        mVolumeManager.sendVolumeChanged(activeDevice, deviceVolume);
    }

    Metadata getCurrentSongInfo() {
        return mMediaPlayerList.getCurrentSongInfo();
    }

    PlayStatus getPlayState(boolean a2dp_event) {
        BluetoothDevice a2dpActiveDevice = (A2dpService.getA2dpService() != null) ?
                A2dpService.getA2dpService().getActiveDevice() : null;
        if (a2dpActiveDevice != null) {
            if (isPlayerStateUpdateBlackListed(a2dpActiveDevice.getAddress(),
                    a2dpActiveDevice.getName()) == true) {
                PlaybackState curState = mMediaPlayerList.getCurrentPlayStatus();
                Log.d(TAG, "MediaController playbackstate: " + curState);
                boolean isPlaying = a2dp_event ?
                        A2dpService.getA2dpService().isA2dpPlaying(a2dpActiveDevice) :
                        mAudioManagerIsPlaying;
                if (isPlaying) {
                    if (curState == null
                            || curState.getState() == PlaybackState.STATE_PAUSED
                            || curState.getState() == PlaybackState.STATE_STOPPED) {
                        Log.d(TAG, "PlaybackState is changed to PLAYING");
                    } else {
                        Log.d(TAG, "PlaybackState is kept as PLAYING");
                    }
                } else {
                    Log.d(TAG, "PlaybackState is changed to PAUSED");
                }

                PlaybackState newState = new PlaybackState.Builder()
                        .setState((isPlaying ? PlaybackState.STATE_PLAYING :
                                        PlaybackState.STATE_PAUSED),
                                (curState == null ? 0L : curState.getPosition()),
                                0.0f)
                        .build();
                return PlayStatus.fromPlaybackState(newState,
                        Long.parseLong(getCurrentSongInfo().duration));
            }
        }

        return PlayStatus.fromPlaybackState(mMediaPlayerList.getCurrentPlayStatus(),
                Long.parseLong(getCurrentSongInfo().duration));
    }

    String getCurrentMediaId() {
        String id = mMediaPlayerList.getCurrentMediaId();
        if (id != null) return id;

        Metadata song = getCurrentSongInfo();
        if (song != null) return song.mediaId;

        // We always want to return something, the error string just makes debugging easier
        return "error";
    }

    List<Metadata> getNowPlayingList() {
        return mMediaPlayerList.getNowPlayingList();
    }

    int getCurrentPlayerId() {
        BluetoothDevice a2dpActiveDevice = (A2dpService.getA2dpService() != null) ?
                A2dpService.getA2dpService().getActiveDevice() : null;
        boolean isBrowsedPlayerForcely = a2dpActiveDevice != null ?
                isBrowsedPlayerBlackListed(a2dpActiveDevice.getAddress(),
                        a2dpActiveDevice.getName()) : false;
        if (DEBUG) {
            Log.d(TAG, "getCurrentPlayerId: isBrowsedPlayerBlackListed "
                    + isBrowsedPlayerForcely);
        }
        return mMediaPlayerList.getCurrentPlayerId(isBrowsedPlayerForcely);
    }

    // TODO (apanicke): Have the Player List also contain info about the play state of each player
    List<PlayerInfo> getMediaPlayerList() {
        BluetoothDevice a2dpActiveDevice = (A2dpService.getA2dpService() != null) ?
                A2dpService.getA2dpService().getActiveDevice() : null;
        boolean isBrowsedPlayerForcely = a2dpActiveDevice != null ?
                isBrowsedPlayerBlackListed(a2dpActiveDevice.getAddress(),
                        a2dpActiveDevice.getName()) : false;
        if (DEBUG) {
            Log.d(TAG, "getMediaPlayerList: isBrowsedPlayerBlackListed "
                    + isBrowsedPlayerForcely);
        }
        return mMediaPlayerList.getMediaPlayerList(isBrowsedPlayerForcely);
    }

    void getPlayerRoot(int playerId, MediaPlayerList.GetPlayerRootCallback cb) {
        BluetoothDevice a2dpActiveDevice = (A2dpService.getA2dpService() != null) ?
                A2dpService.getA2dpService().getActiveDevice() : null;
        boolean isBrowsedPlayerForcely = a2dpActiveDevice != null ?
                isBrowsedPlayerBlackListed(a2dpActiveDevice.getAddress(),
                        a2dpActiveDevice.getName()) : false;
        if (DEBUG) {
            Log.d(TAG, "getPlayerRoot: isBrowsedPlayerBlackListed "
                    + isBrowsedPlayerForcely);
        }
        mMediaPlayerList.getPlayerRoot(playerId, isBrowsedPlayerForcely, cb);
    }

    void getFolderItems(int playerId, String mediaId, MediaPlayerList.GetFolderItemsCallback cb) {
        mMediaPlayerList.getFolderItems(playerId, mediaId, cb);
    }

    void playItem(int playerId, boolean nowPlaying, String mediaId) {
        // NOTE: playerId isn't used if nowPlaying is true, since its assumed to be the current
        // active player
        mMediaPlayerList.playItem(playerId, nowPlaying, mediaId);
    }

    // TODO (apanicke): Handle key events here in the service. Currently it was more convenient to
    // handle them there but logically they make more sense handled here.
    void sendMediaKeyEvent(int event, boolean pushed) {
        if (DEBUG) Log.d(TAG, "getMediaKeyEvent: event=" + event + " pushed=" + pushed);
        mMediaPlayerList.sendMediaKeyEvent(event, pushed);
    }

    void setActiveDevice(BluetoothDevice device) {
        Log.i(TAG, "setActiveDevice: device=" + device);
        if (device == null) {
            Log.wtfStack(TAG, "setActiveDevice: could not find device " + device);
        }
        A2dpService.getA2dpService().setActiveDevice(device);
    }

    /**
     * Dump debugging information to the string builder
     */
    public void dump(StringBuilder sb) {
        sb.append("\nProfile: AvrcpTargetService:\n");
        if (sInstance == null) {
            sb.append("AvrcpTargetService not running");
            return;
        }

        StringBuilder tempBuilder = new StringBuilder();
        if (mMediaPlayerList != null) {
            mMediaPlayerList.dump(tempBuilder);
        } else {
            tempBuilder.append("\nMedia Player List is empty\n");
        }

        mVolumeManager.dump(tempBuilder);

        // Tab everything over by two spaces
        sb.append(tempBuilder.toString().replaceAll("(?m)^", "  "));
    }

    private static class AvrcpTargetBinder extends IBluetoothAvrcpTarget.Stub
            implements IProfileServiceBinder {
        private AvrcpTargetService mService;

        AvrcpTargetBinder(AvrcpTargetService service) {
            mService = service;
        }

        @Override
        public void cleanup() {
            mService = null;
        }

        @Override
        public void sendVolumeChanged(int volume) {
            if (!Utils.checkCaller()) {
                Log.w(TAG, "sendVolumeChanged not allowed for non-active user");
                return;
            }

            if (mService == null) {
                return;
            }

            mService.sendVolumeChanged(volume);
        }
    }
}
